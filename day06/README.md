## Скрипт для создания директорий ex00-exNN

<a href=mkalldirs.sh>mkalldirs.sh</a>

## Инструкция по развертыванию виртуальной машины + PostgreSQL/pgAdmin в контейнерах Docker

<a href="https://gitlab.com/school218063421/sql-projects/s21-sql-bootcamp/-/blob/main/README.md" target=_blank>ЖМАКНИ СЮДА</a>

## Запросы
- ex00
  ```sql
  -- Удаляем таблицу, если ранее была создана (для чистоты эксперимента)
  DROP TABLE IF EXISTS person_discounts;

  CREATE TABLE person_discounts
  (
    -- Создаем поля
    id BIGINT PRIMARY KEY,
    person_id BIGINT NOT NULL,
    pizzeria_id BIGINT NOT NULL,
    discount_percent NUMERIC(7,2) NOT NULL DEFAULT '0.00',
    -- Создаем шаблоны связей полей создаваемой таблицы с соответствующими полями связанных таблиц
    CONSTRAINT fk_person_discounts_person_id Foreign Key (person_id) REFERENCES person(id),
    CONSTRAINT fk_person_discounts_pizzeria_id Foreign Key (pizzeria_id) REFERENCES pizzeria(id)
  );
  ```
- ex01
  ```sql
  INSERT INTO person_discounts (id, person_id, pizzeria_id, discount_percent)
    -- ROW_NUMBER() OVER(): 
    -- Генерирует уникальный идентификатор для каждой строки в наборе результатов.
    SELECT 
        ROW_NUMBER( ) OVER ( ) AS id,
        ORDR.person_id,
        MNU.pizzeria_id,
        (
            CASE 
                WHEN COUNT(ORDR.person_id) = 1 THEN 10.50
                WHEN COUNT(ORDR.person_id) = 2 THEN 22.00
                ELSE 30.00
            END
        ) AS discount_percent
    FROM person_order ORDR
        JOIN menu MNU ON ORDR.menu_id = MNU.id
    GROUP BY ORDR.person_id, MNU.pizzeria_id
    ORDER BY ORDR.person_id;

  -- Проверяем заполнение
  SELECT * FROM person_discounts;
  ```
- ex02
  ```sql
  SELECT 
    PRS.name, 
    MNU.pizza_name, 
    MNU.price,
    -- ::REAL убирает из числа лишние нули, 
    -- но оставляет дробную часть, если она существует 
    (MNU.price - MNU.price*DSC.discount_percent/100)::REAL AS discount_price,
    PZR.name AS pizzeria_name
  FROM person_order PO
    JOIN person PRS ON PRS.id = PO.person_id
    JOIN menu MNU ON MNU.id = PO.menu_id
    JOIN pizzeria PZR ON MNU.pizzeria_id = PZR.id
    JOIN person_discounts DSC ON PO.person_id = DSC.person_id AND MNU.pizzeria_id = DSC.pizzeria_id
  ORDER BY PRS.name, pizzeria_name;
  ```
- ex03
  ```sql
  CREATE UNIQUE INDEX idx_person_discounts_unique ON person_discounts(person_id, pizzeria_id);

  SET enable_seqscan TO OFF;
  EXPLAIN ANALYZE
    SELECT * FROM person_discounts
    WHERE person_id = 6 AND pizzeria_id = 4;
  ```
- ex04
  ```sql
  ALTER TABLE person_discounts RENAME COLUMN discount_percent TO discount;

  ALTER TABLE person_discounts ADD CONSTRAINT ch_nn_person_id CHECK (person_id IS NOT NULL);
  ALTER TABLE person_discounts ADD CONSTRAINT ch_nn_pizzeria_id CHECK (pizzeria_id IS NOT NULL);
  ALTER TABLE person_discounts ADD CONSTRAINT ch_nn_discount CHECK (discount IS NOT NULL);
  ALTER TABLE person_discounts ALTER COLUMN discount SET DEFAULT 0.00;
  ALTER TABLE person_discounts ADD CONSTRAINT ch_range_discount CHECK (discount BETWEEN 0.00 AND 100.00);
  ```
- ex05
  ```sql
  COMMENT ON TABLE person_discounts IS 'Таблица, содержащая информацию об ID заказчиков и пицерий, а также персональные скидки в конкретных пицериях';
  COMMENT ON COLUMN person_discounts.person_id IS 'ID заказчиков';
  COMMENT ON COLUMN person_discounts.pizzeria_id IS 'ID пиццерий';
  COMMENT ON COLUMN person_discounts.discount IS 'Размер персональной скидки в конкретной пицерии в процентах';
  ```
- ex06
  ```sql
  CREATE SEQUENCE seq_person_discounts AS BIGINT START WITH 1 OWNED BY person_discounts.id;
  ALTER TABLE person_discounts ALTER COLUMN id SET DEFAULT nextval('seq_person_discounts');

  -- Установка следующего значения последовательности
  SELECT setval('seq_person_discounts', (SELECT MAX(id) + 1 FROM person_discounts)) AS nextvalue;
  ```

