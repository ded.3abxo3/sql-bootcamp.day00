## Скрипт для создания директорий ex00-exNN

<a href=mkalldirs.sh>mkalldirs.sh</a>

## Инструкция по развертыванию виртуальной машины + PostgreSQL/pgAdmin в контейнерах Docker

<a href="https://gitlab.com/school218063421/sql-projects/s21-sql-bootcamp/-/blob/main/README.md" target=_blank>ЖМАКНИ СЮДА</a>

## Запросы
- ex00
  ```sql
  -- CREATE INDEX IF NOT EXISTS idx_{table_name}_{column_name} ON table_name(column_name)
  CREATE INDEX IF NOT EXISTS idx_menu_pizzeria_id ON menu(pizzeria_id);
  CREATE INDEX IF NOT EXISTS idx_person_visits_pizzeria_id ON person_visits(pizzeria_id);
  CREATE INDEX IF NOT EXISTS idx_person_visits_person_id ON person_visits(person_id);
  CREATE INDEX IF NOT EXISTS idx_person_order_person_id ON person_order(person_id);
  CREATE INDEX IF NOT EXISTS idx_person_order_menu_id ON person_order(menu_id);
  ```
- ex01
  ```sql
  -- Может использовать последовательное сканирование без использования индексов. 
  -- Потенциально увеличивает время обработки запроса
  SET enable_seqscan TO ON;
  
  EXPLAIN ANALYZE 
    SELECT MNU.pizza_name, PZR.name FROM menu MNU
    JOIN pizzeria PZR on MNU.pizzeria_id= PZR.id;

  -- Использование индексов. 
  -- Потенциально уменьшает время обработки запроса
  SET enable_seqscan TO OFF;
  
  EXPLAIN ANALYZE
    SELECT MNU.pizza_name, PZR.name FROM menu MNU
    JOIN pizzeria PZR on MNU.pizzeria_id= PZR.id; 
  ```
- ex02
  ```sql
  CREATE INDEX idx_person_name  ON person(UPPER(name));

  SET enable_seqscan TO ON;
  EXPLAIN ANALYSE
    SELECT * FROM person
    WHERE UPPER(name) LIKE 'DENIS';

  SET enable_seqscan TO OFF;
  EXPLAIN ANALYSE
    SELECT * FROM person
    WHERE UPPER(name) LIKE 'DENIS';
  ```
- ex03
  ```sql
  CREATE INDEX IF NOT EXISTS idx_person_order_multi ON person_order(person_id, menu_id, order_date);

  SET enable_seqscan TO ON;
  EXPLAIN ANALYZE
    SELECT person_id, menu_id,order_date
    FROM person_order
    WHERE person_id = 8 AND menu_id = 19;

  -- Выключаем последовательное сканирование, выборка только по Index Only
  SET enable_seqscan TO OFF;  
  EXPLAIN ANALYZE
    SELECT person_id, menu_id,order_date
    FROM person_order
    WHERE person_id = 8 AND menu_id = 19;
  ```
- ex04
  ```sql
  CREATE INDEX IF NOT EXISTS idx_menu_unique ON menu(pizzeria_id, pizza_name);

  SET enable_seqscan TO ON;
  EXPLAIN ANALYZE
    SELECT pizzeria_id, pizza_name FROM menu
    WHERE pizza_name LIKE '%pizza%' AND pizzeria_id = 1;

  SET enable_seqscan TO OFF;
  EXPLAIN ANALYZE
    SELECT pizzeria_id, pizza_name FROM menu
    WHERE pizza_name LIKE '%pizza%' AND pizzeria_id = 1;
  ```
- ex05
  ```sql
  CREATE UNIQUE INDEX IF NOT EXISTS  idx_person_order_order_date ON person_order(person_id, menu_id)
    WHERE order_date = '2022-01-01';

  SET enable_seqscan TO OFF;
  EXPLAIN ANALYZE
    SELECT person_id, menu_id FROM person_order
        WHERE order_date = '2022-01-01';
  ```
- ex06
  ```sql
  CREATE INDEX IF NOT EXISTS idx_1 ON pizzeria(rating);

  SET enable_seqscan TO ON;
  EXPLAIN ANALYZE
    SELECT
    m.pizza_name AS pizza_name,
    max(rating) OVER (PARTITION BY rating ORDER BY rating ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS k
    FROM  menu m
        INNER JOIN pizzeria pz ON m.pizzeria_id = pz.id
    ORDER BY 1,2;

  -- ВКЛЮЧАЕМ УСКОРЕНИЕ: Блиц - скорость без границ :)))
  SET enable_seqscan TO OFF;
  EXPLAIN ANALYZE
    SELECT
    m.pizza_name AS pizza_name,
    max(rating) OVER (PARTITION BY rating ORDER BY rating ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS k
    FROM  menu m
        INNER JOIN pizzeria pz ON m.pizzeria_id = pz.id
    ORDER BY 1,2;
  ```

