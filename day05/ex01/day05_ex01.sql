SET enable_seqscan TO ON; -- Может использовать последовательное сканирование без использования индексов. Потенциально увеличивает время обработки запроса
EXPLAIN ANALYZE 
    SELECT MNU.pizza_name, PZR.name FROM menu MNU
    JOIN pizzeria PZR on MNU.pizzeria_id= PZR.id;

SET enable_seqscan TO OFF; -- Использование индексов. Потенциально уменьшает время обработки запроса
EXPLAIN ANALYZE
    SELECT MNU.pizza_name, PZR.name FROM menu MNU
    JOIN pizzeria PZR on MNU.pizzeria_id= PZR.id; 
    
