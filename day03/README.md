## Скрипт для создания директорий ex00-exNN

<a href=mkalldirs.sh>mkalldirs.sh</a>

## Инструкция по развертыванию виртуальной машины + PostgreSQL/pgAdmin в контейнерах Docker

<a href=../README.md>../README.md</a>

## Запросы
- ex00
  ```sql
  SELECT MN.pizza_name, MN.price, PZR.name AS pizzeria_name, PV.visit_date FROM menu MN
  LEFT JOIN pizzeria PZR ON MN.pizzeria_id = PZR.id
  LEFT JOIN person_visits PV ON PZR.id = PV.pizzeria_id
  LEFT JOIN person PRS ON PV.person_id = PRS.id
  WHERE PRS.name LIKE 'Kate' AND (MN.price BETWEEN 800 AND 1000)
  ORDER BY MN.pizza_name, MN.price, pizzeria_name;
  ```
- ex01
  ```sql
  SELECT id AS menu_id FROM menu MN
  WHERE NOT EXISTS
    (SELECT menu_id FROM person_order WHERE menu_id = MN.id)
  ORDER BY menu_id;
  ```
- ex02
  ```sql
  SELECT 
    MN.pizza_name, 
    MN.price, 
    PZR.name AS pizzeria_name
  FROM 
    menu MN,
    pizzeria PZR
  WHERE 
    MN.pizzeria_id = PZR.id
    AND NOT EXISTS (
        SELECT * FROM person_order PO
        WHERE PO.menu_id = MN.id
    )
  ORDER BY 
    MN.pizza_name, MN.price;
  ```
- ex03
  ```sql
  (SELECT pizzeria.name AS pizzeria_name
  FROM person_visits
    JOIN pizzeria ON person_visits.pizzeria_id = pizzeria.id
    JOIN person ON person_visits.person_id = person.id
  WHERE person.gender = 'female'
  EXCEPT ALL
  SELECT pizzeria.name AS pizzeria_name
  FROM person_visits
    JOIN pizzeria ON person_visits.pizzeria_id = pizzeria.id
    JOIN person ON person_visits.person_id = person.id
  WHERE person.gender = 'male'
  )
  UNION ALL
  (SELECT pizzeria.name AS pizzeria_name
  FROM person_visits
    JOIN pizzeria ON person_visits.pizzeria_id = pizzeria.id
    JOIN person ON person_visits.person_id = person.id
  WHERE person.gender = 'male'
  EXCEPT ALL
  SELECT pizzeria.name AS pizzeria_name
  FROM person_visits
    JOIN pizzeria ON person_visits.pizzeria_id = pizzeria.id
    JOIN person ON person_visits.person_id = person.id
  WHERE person.gender = 'female')
  ORDER BY pizzeria_name;
  ```
- ex04
  ```sql
  (SELECT PZ.name AS pizzeria_name
    FROM menu MN
        JOIN pizzeria PZ ON MN.pizzeria_id = PZ.id
        JOIN person_order PO ON MN.id = PO.menu_id
        JOIN person PRS ON PRS.id = PO.person_id
    WHERE PRS.gender = 'female'
    EXCEPT
    SELECT PZ.name AS pizzeria_name
    FROM menu MN
        JOIN pizzeria PZ ON MN.pizzeria_id = PZ.id
        JOIN person_order PO ON MN.id = PO.menu_id
        JOIN person PRS ON PRS.id = PO.person_id
    WHERE PRS.gender = 'male'
  ) 
  UNION
  (SELECT PZ.name AS pizzeria_name
    FROM menu MN
        JOIN pizzeria PZ ON MN.pizzeria_id = PZ.id
        JOIN person_order PO ON MN.id = PO.menu_id
        JOIN person PRS ON PRS.id = PO.person_id
    WHERE PRS.gender = 'male'
    EXCEPT
    SELECT PZ.name AS pizzeria_name
    FROM menu MN
        JOIN pizzeria PZ ON MN.pizzeria_id = PZ.id
        JOIN person_order PO ON MN.id = PO.menu_id
        JOIN person PRS ON PRS.id = PO.person_id
    WHERE PRS.gender = 'female'
  )
  ORDER BY pizzeria_name;
  ```
- ex05
  ```sql
  SELECT PZ.name AS pizzeria_name
  FROM person_visits PV
    FULL JOIN person_order PO ON PO.order_date = PV.visit_date
    JOIN pizzeria PZ ON PZ.id = PV.pizzeria_id
    JOIN person PRS ON PRS.id = PV.person_id
  WHERE PRS.name LIKE 'Andrey'
    AND PO.id IS NULL
  ORDER BY pizzeria_name;
  ```
- ex06
  ```sql
  SELECT DISTINCT
    MN1.pizza_name,
    PZ1.name AS pizzeria_name_1,
    PZ2.name AS pizzeria_name_2,
    MN1.price
  FROM menu MN1
    JOIN menu MN2 ON MN1.price = MN2.price
    JOIN pizzeria PZ1 ON MN1.pizzeria_id = PZ1.id
    JOIN pizzeria PZ2 ON MN2.pizzeria_id = PZ2.id
  WHERE 
    MN1.id > MN2.id AND PZ1.name <> PZ2.name
  ORDER BY MN1.pizza_name;
  ```
- ex07
  ```sql
  INSERT INTO menu (id, pizzeria_id, pizza_name, price) VALUES (19, 2, 'greek pizza', 800);
  ```
- ex08
  ```sql
  INSERT INTO menu (id, pizzeria_id, pizza_name, price)
  VALUES (
		(SELECT MAX(id) + 1 FROM menu),
  		(SELECT id FROM pizzeria WHERE name LIKE 'Dominos'),
  		'sicilian pizza',
  		900
  );
  ```
- ex09
  ```sql
  INSERT INTO person_visits (id, person_id, pizzeria_id, visit_date)
  VALUES (
		(SELECT MAX(id) + 1 FROM person_visits),
        (SELECT id FROM person WHERE name LIKE 'Denis'),
  		(SELECT id FROM pizzeria WHERE name LIKE 'Dominos'),
  		'2022-02-24'
  );
  ```
  ```sql
  INSERT INTO person_visits (id, person_id, pizzeria_id, visit_date)
  VALUES (
		(SELECT MAX(id) + 1 FROM person_visits),
        (SELECT id FROM person WHERE name LIKE 'Irina'),
  		(SELECT id FROM pizzeria WHERE name LIKE 'Dominos'),
  		'2022-02-24'
  );
  ```
- ex10
  ```sql
  INSERT INTO person_order (id, person_id, menu_id, order_date)
  VALUES (
		(SELECT MAX(id) + 1 FROM person_order),
        (SELECT id FROM person WHERE name LIKE 'Denis'),
  		(SELECT id FROM menu WHERE pizza_name LIKE 'sicilian pizza'),
  		'2022-02-24'
  );
  ```
  ```sql
  INSERT INTO person_order (id, person_id, menu_id, order_date)
  VALUES (
		(SELECT MAX(id) + 1 FROM person_order),
        (SELECT id FROM person WHERE name LIKE 'Irina'),
  		(SELECT id FROM menu WHERE pizza_name LIKE 'sicilian pizza'),
  		'2022-02-24'
  );
  ```
- ex11
  ```sql
  UPDATE menu 
  SET
    price = CAST(price - price*0.1 AS INT)
  WHERE
    pizza_name LIKE 'greek pizza';
  ```
- ex12
  ```sql
  INSERT INTO person_order(id, person_id, menu_id, order_date)
  SELECT
		GENERATE_SERIES(
      (SELECT MAX(id) FROM person_order) + 1,
      (SELECT MAX(id) FROM person_order) + (SELECT COUNT(id) FROM person),
      1),
    GENERATE_SERIES(
      (SELECT MIN(id) FROM person), 
      (SELECT MAX(id) FROM person),
      1),
    (SELECT id FROM menu WHERE pizza_name = 'greek pizza'),
    '2022-02-25';
  ```
- ex13
  ```sql
  DELETE FROM person_order
  WHERE order_date='2022-02-25';
  ```
  ```sql
  DELETE FROM menu
  WHERE pizza_name='greek pizza';
  ```