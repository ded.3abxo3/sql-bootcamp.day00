## Скрипт для создания директорий ex00-exNN

<a href=mkalldirs.sh>mkalldirs.sh</a>

## Инструкция по развертыванию виртуальной машины + PostgreSQL/pgAdmin в контейнерах Docker

<a href="https://gitlab.com/school218063421/sql-projects/s21-sql-bootcamp/-/blob/main/README.md" target=_blank>ЖМАКНИ СЮДА</a>

## Запросы
- ex00
  ```sql
  SELECT person_id, COUNT(id) AS count_of_visits
  FROM person_visits
  GROUP BY person_id
  ORDER BY 2 DESC, 1 ASC;
  ```
- ex01
  ```sql
  SELECT person.name, COUNT(*) AS count_of_visits
  FROM person_visits
    JOIN person ON person.id = person_visits.person_id
  GROUP BY person.name
  ORDER BY 2 DESC, 1 ASC
  LIMIT 4;
  ```
- ex02
  ```sql
  (
    SELECT pizzeria.name, COUNT(person_order.id) as count,  'order' AS action_type
  FROM person_order
    JOIN menu ON person_order.menu_id = menu.id
    JOIN pizzeria ON menu.pizzeria_id = pizzeria.id
  GROUP BY pizzeria.name
  ORDER BY count DESC, pizzeria.name
  LIMIT 3
  )
  UNION
  (
  SELECT pizzeria.name, COUNT(person_visits.id) as count,  'visit' AS action_type
  FROM person_visits
    JOIN pizzeria ON person_visits.pizzeria_id = pizzeria.id
  GROUP BY pizzeria.name
  ORDER BY count DESC, pizzeria.name
  LIMIT 3
  )
  ORDER BY action_type, count DESC;
  ```
- ex03
  ```sql
  WITH selected_orders AS
        (SELECT pizzeria.name, count(*) count, 'order' as action_type
        FROM person_order
            JOIN menu ON menu.id=person_order.menu_id
            JOIN pizzeria on pizzeria.id=menu.pizzeria_id
        GROUP BY pizzeria.name
        ORDER BY count DESC, pizzeria.name),
    selected_visits AS
        (SELECT pizzeria.name, count(*) count, 'visit' as action_type
        FROM person_visits
            JOIN pizzeria on pizzeria.id=person_visits.pizzeria_id
        GROUP BY pizzeria.name
        ORDER BY count DESC, pizzeria.name)
  SELECT selected_orders.name, (selected_orders.count + selected_visits.count) as total_count 
  FROM selected_orders
    JOIN selected_visits ON selected_orders.name=selected_visits.name
  ORDER BY total_count DESC, name ASC;
  ```
- ex04
  ```sql
  SELECT person.name, COUNT(person_visits.id) AS total_count
  FROM person
    JOIN person_visits ON person_visits.person_id = person.id
  GROUP BY person.name
  HAVING COUNT(person_visits.id) > 3;
  ```
- ex05
  ```sql
  SELECT DISTINCT person.name 
  FROM person_order
    LEFT JOIN person ON person.id=person_order.person_id
  ORDER BY 1;
  ```
- ex06
  ```sql
  SELECT pizzeria.name, COUNT(*) AS count_of_orders, ROUND(AVG(menu.price), 2) AS average_price, MAX(menu.price) AS max_price, MIN(menu.price) AS min_price
  FROM menu
    JOIN person_order ON menu.id = person_order.menu_id
    JOIN pizzeria ON menu.pizzeria_id = pizzeria.id
  GROUP BY pizzeria.name
  ORDER BY 1;
  ```
- ex07
  ```sql
  SELECT ROUND(AVG(rating), 4) AS global_rating
  FROM pizzeria;
  ```
- ex08
  ```sql
  SELECT person.address, pizzeria.name, COUNT(*) as count_of_orders
  FROM person
    JOIN person_order ON person_order.person_id = person.id
    JOIN menu ON person_order.menu_id = menu.id
    JOIN pizzeria ON menu.pizzeria_id = pizzeria.id
  GROUP BY person.address, pizzeria.name
  ORDER BY 1, 2;
  ```
- ex09
  ```sql
  SELECT address, 
    ROUND(MAX(age)-MIN(age)*1.0/MAX(age), 2) AS formula, 
    ROUND(AVG(age), 2) AS average, 
    CASE WHEN MAX(age)-MIN(age)*1.0/MAX(age) > AVG(age) THEN true ELSE false END
  FROM person
  GROUP BY address
  ORDER BY 1;
  ```
