## Скрипт для создания директорий ex00-exNN

<a href=mkalldirs.sh>mkalldirs.sh</a>

## Инструкция по развертыванию виртуальной машины + PostgreSQL/pgAdmin в контейнерах Docker

<a href=../README.md>../README.md</a>

## Запросы
- ex00
  ```bash
    SELECT id AS object_id, pizza_name AS object_name FROM menu
    UNION
    SELECT id AS object_id, name AS object_name FROM person
    ORDER BY object_id, object_name;
  ```
- ex01
  ```bash
    (SELECT name AS object_name FROM person ORDER BY name)
    UNION ALL
    (SELECT pizza_name AS object_name FROM menu ORDER BY pizza_name);
  ```
- ex02
  ```bash
    SELECT pizza_name FROM menu
    UNION SELECT pizza_name FROM menu
    ORDER BY pizza_name DESC;
  ```
- ex03
  ```bash
    SELECT order_date AS action_date, person_id FROM person_order
    INTERSECT
    SELECT visit_date AS action_date, person_id FROM person_visits
    ORDER BY action_date, person_id DESC;
  ```
- ex04
  ```bash
    SELECT person_id FROM person_order WHERE order_date='2022-01-07'
    EXCEPT ALL
    SELECT person_id FROM person_visits WHERE visit_date='2022-01-07';
  ```
- ex05
  ```bash
    SELECT 
	    person.id, person.name, age, gender, address, 
	    pizzeria.id, pizzeria.name, rating 
    FROM 
	    person, pizzeria 
    ORDER BY 
	    person.id, pizzeria.id;
  ```
- ex06
  ```bash
    SELECT 
	    action_date, name 
    FROM 
	    person,
	    (SELECT order_date AS action_date, person_id FROM person_order
	    INTERSECT
	    SELECT visit_date AS action_date, person_id FROM person_visits) 
		    AS dynamic_united_table
    WHERE 
	    dynamic_united_table.person_id = person.id
    ORDER BY 
	    action_date, person.name DESC;
  ```
- ex07
  ```bash
    SELECT person_order.order_date AS order_date, 
	    CONCAT(person.name,' (age:',person.age,')') AS person_information
    FROM person
	    JOIN person_order ON person_order.person_id=person.id
    ORDER BY person_order.order_date, person.name;
  ```
- ex08
  ```bash
	SELECT person_order.order_date AS order_date, 
	    CONCAT(person.name,' (age:',person.age,')') AS person_information
    FROM person
	    NATURAL JOIN (SELECT person_id AS id, order_date FROM person_order) AS person_order
    ORDER BY person_order.order_date, person.name;
  ```
- ex09
  ```bash
	SELECT name
    FROM pizzeria
	WHERE id NOT IN (SELECT DISTINCT pizzeria_id FROM person_visits);
  ```
  ```bash
	SELECT name
    FROM pizzeria
	WHERE NOT EXISTS (SELECT DISTINCT pizzeria_id FROM person_visits WHERE pizzeria_id=pizzeria.id);
  ```
- ex10
  ```bash
    SELECT 
	    PERS_T.name AS person_name, 
	    MENU_T.pizza_name AS pizza_name,
	    PIZZERIA_T.name AS pizzeria_name
    FROM person_order ORDERS_T
	    JOIN person PERS_T ON PERS_T.id = ORDERS_T.person_id
	    JOIN menu MENU_T ON MENU_T.id = ORDERS_T.menu_id
	    JOIN pizzeria PIZZERIA_T ON MENU_T.pizzeria_id = PIZZERIA_T.id
    ORDER BY person_name, pizza_name, pizzeria_name;
  ```