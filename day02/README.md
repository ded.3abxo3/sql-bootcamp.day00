## Скрипт для создания директорий ex00-exNN

<a href=mkalldirs.sh>mkalldirs.sh</a>

## Инструкция по развертыванию виртуальной машины + PostgreSQL/pgAdmin в контейнерах Docker

<a href=../README.md>../README.md</a>

## Запросы
- ex00
  ```bash
  SELECT name, rating 
  FROM pizzeria PIZZERIA
  LEFT JOIN person_visits VISITS 
    ON VISITS.pizzeria_id = PIZZERIA.id 
    WHERE VISITS.pizzeria_id IS NULL;
  ```
- ex01
  ```bash
  SELECT DINTERVAL.date as missing_date
  FROM GENERATE_SERIES('2022-01-01', '2022-01-10', interval '1 day') as DINTERVAL
  LEFT JOIN 
    (SELECT DISTINCT visit_date
    FROM person_visits
    WHERE person_id = 1 OR person_id = 2) as VISIT_ID_1or2 
  ON DINTERVAL.date=VISIT_ID_1or2.visit_date
  WHERE VISIT_ID_1or2.visit_date is NULL
  ORDER BY missing_date;
  ```
- ex02
  ```bash
  SELECT 
    COALESCE(PERSON.name,'-') as person_name, 
    VISIT.visit_date as visit_date, 
    COALESCE(PIZZERIA.name,'-') as pizzeria_name 
  FROM 
    (SELECT * from person_visits WHERE visit_date BETWEEN '2022-01-01' AND '2022-01-03') as VISIT
  FULL JOIN person PERSON ON VISIT.person_id = PERSON.id 
  FULL JOIN pizzeria PIZZERIA ON VISIT.pizzeria_id = PIZZERIA.id
  ORDER BY person_name,visit_date,pizzeria_name;
  ```
- ex03
  ```bash
  WITH cte(missing_date) AS 
    (SELECT DI.DATE as missing_date
    FROM GENERATE_SERIES('2022-01-01', '2022-01-10', interval '1 day') as DI)
  SELECT cte.missing_date 
  FROM cte
  LEFT JOIN 
    (SELECT DISTINCT pv.visit_date
    FROM person_visits pv
    WHERE pv.person_id = 1 OR pv.person_id = 2) as VISITS
  ON cte.missing_date = VISITS.visit_date
  WHERE VISITS.visit_date IS NULL
  ORDER BY cte.missing_date;
  ```
- ex04
  ```bash
  SELECT 
    M.pizza_name, 
    P.name AS pizzeria_name,
    M.price
  FROM menu M
  LEFT JOIN pizzeria P ON P.id = M.pizzeria_id 
  WHERE 
    M.pizza_name LIKE 'mushroom%' OR M.pizza_name LIKE 'pepperoni%'
  ORDER BY M.pizza_name, pizzeria_name;
  ```
- ex05
  ```bash
  SELECT name FROM person
  WHERE
    age > 25 AND gender = 'female'
  ORDER BY name;
  ```
- ex06
  ```bash
  SELECT 
    MN.pizza_name as pizza_name, 
    PZR.name as pizzeria_name
  FROM menu MN
  LEFT JOIN pizzeria PZR ON MN.pizzeria_id = PZR.id
  LEFT JOIN person_order ORD ON MN.id = ORD.menu_id
  LEFT JOIN person PRS ON ORD.person_id = PRS.id 
    WHERE PRS.name LIKE 'Denis' OR PRS.name LIKE 'Anna'
  ORDER BY pizza_name, pizzeria_name;
  ```
- ex07
  ```bash
  SELECT PZR.name AS pizzeria_name
  FROM pizzeria PZR
  LEFT JOIN person_visits PVS ON PZR.id = PVS.pizzeria_id
  LEFT JOIN person PRS ON PVS.person_id = PRS.id
  LEFT JOIN menu MN ON PZR.id = MN.pizzeria_id
  WHERE PVS.visit_date = '2022-01-08' AND PRS.name LIKE 'Dmitriy' AND MN.price < 800
  ORDER BY PZR.name;
  ```
- ex08
  ```bash
  SELECT DISTINCT PRS.name FROM person PRS
  LEFT JOIN person_order ORD ON PRS.id = ORD.person_id
  LEFT JOIN menu MN ON MN.id = ORD.menu_id
  WHERE PRS.gender = 'male' 
    AND (PRS.address LIKE 'Moscow' OR PRS.address LIKE 'Samara')
    AND (MN.pizza_name LIKE 'mushroom%' OR MN.pizza_name LIKE 'pepperoni%')
  ORDER BY PRS.name DESC;
  ```
- ex09
  ```bash
  SELECT DISTINCT PRS.name 
  FROM person PRS
  LEFT JOIN person_order ORD ON PRS.id = ORD.person_id
  LEFT JOIN menu MN ON MN.id = ORD.menu_id
  WHERE PRS.gender = 'female'
    AND (
        MN.pizza_name LIKE '%cheese%'
        OR MN.pizza_name LIKE '%pepperoni%'
    )
  GROUP BY PRS.name
    HAVING SUM(CASE WHEN MN.pizza_name LIKE '%cheese%' THEN 1 ELSE 0 END) > 0
        AND SUM(CASE WHEN MN.pizza_name LIKE '%pepperoni%' THEN 1 ELSE 0 END) > 0
  ORDER BY PRS.name;
  ```
- ex10
  ```bash
  WITH RankedPeople AS 
    (SELECT id, name, address
    FROM person)
  SELECT 
    p1.name AS person_name1,
    p2.name AS person_name2,
    p1.address AS common_address
  FROM RankedPeople p1
  CROSS JOIN RankedPeople p2 
    WHERE p1.address = p2.address AND p1.id > p2.id
  ORDER BY p1.name, p2.name, p1.address;
  ```
