## Скрипт для создания директорий ex00-exNN

<a href=mkalldirs.sh>mkalldirs.sh</a>

## Инструкция по развертыванию виртуальной машины + PostgreSQL/pgAdmin в контейнерах Docker

<a href=../README.md>../README.md</a>

## Запросы
- ex00
  ```bash
  SELECT name, age FROM person WHERE address='Kazan';
  ```
- ex01
  ```bash
  SELECT name, age FROM person WHERE address='Kazan' AND gender='female' ORDER BY name ASC;
  ```
- ex02
  ```bash 
  SELECT name, rating FROM pizzeria WHERE rating>=3.5 AND rating<=5 ORDER BY rating ASC;
  SELECT name, rating FROM pizzeria WHERE rating BETWEEN 3.5 AND 5 ORDER BY rating ASC;
  ```
- ex03
  ```bash
  SELECT DISTINCT person_id FROM person_visits WHERE (visit_date BETWEEN '2022-01-06' AND '2022-01-09') OR pizzeria_id=2 ORDER BY person_id DESC;
  ```
- ex04
  ```bash
  SELECT CONCAT(name, ' (', 'age:', age, ',gender:', '''', gender, '''', ',address:', '''', address, '''', ')') 
  AS person_information
  FROM person
  ORDER BY person_information;
  ```
- ex05
  ```bash
  SELECT
  (SELECT name FROM person WHERE id = person_order.person_id) AS person_name
  FROM person_order
  WHERE (menu_id='13' OR menu_id='14' OR menu_id='18') AND order_date='2022-01-07';
  ```
- ex06
  ```bash
  SELECT
  (SELECT name FROM person WHERE id = person_order.person_id) AS person_name,
    CASE 
        WHEN (SELECT name FROM person WHERE id = person_order.person_id) = 'Denis' THEN TRUE 
        ELSE FALSE 
    END AS check_name
  FROM person_order
  WHERE (menu_id='13' OR menu_id='14' OR menu_id='18') AND order_date='2022-01-07';
  ```
- ex07
  ```bash
  SELECT id, name, 
    CASE 
        WHEN age >= 10 AND age <= 20 THEN 'interval #1'
        WHEN age > 20 AND age < 24 THEN 'interval #2'
        ELSE 'interval #3'
        END AS interval_info
  FROM person
  ORDER BY interval_info;
  ```
- ex08
  ```bash
  SELECT *
  FROM person_order WHERE id%2 = 0
  ORDER BY id;
  ```
- ex09
  ```bash
  SELECT 
    (SELECT name FROM person WHERE id = pv.person_id) AS person_name,
    (SELECT name FROM pizzeria WHERE id = pv.pizzeria_id) AS pizzeria_name
  FROM (SELECT * FROM person_visits WHERE visit_date BETWEEN '2022-01-07' AND '2022-01-09') AS pv
  ORDER BY person_name, pizzeria_name DESC; 
  ```