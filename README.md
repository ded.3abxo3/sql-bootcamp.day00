# Установка и настройка PostgreSQL + pgAdmin на Ubuntu Server

## 1. Разворачиваем PostgreSQL + pgAdmin
1. Устанавливаем на виртуальную машину OS Ubuntu 22.04 LTS без графического интерфейса
2. Пробрасываем мост в основную хост-сеть, т.е.:
<br>2.1. Добавляем сетевую карту типа Bridge в настройках VBox
<br>2.2. Настраиваем netplan таким образом, чтобы мост смотрел в основную хост сеть. 
<br>В моем случае адрес сервера в хост-сети 192.168.1.25'
<br><img src="img/netplan.png">
3. Устанавливаем на сервер docker + docker-composer (Например, как тут: <a href="https://selectel.ru/blog/docker-install-ubuntu/">https://selectel.ru/blog/docker-install-ubuntu/</a>)
4. Клонируем репозиторий в папку на сервере
5. Скачиваем образы и запускаем контейнеры командой `docker-compose up -d`. Внимание! Убедитесь, что команда запускается из той папки, где лежит скачанный из репозитория файл <a href="docker-compose.yml">docker-compose.yml</a>
6. После установки проверяем, запущены ли контейнеры `docker ps`. 
<br>Выглядеть результат должен примерно вот так:
<br><img src="img/img01.png">
7. Пробуем зайти на страницу pgAdmin. В файле <a href="docker-compose.yml">docker-compose.yml</a> заданы следующие данные для входа в pgAdmin:
<br><b>Логин: ded.3abxo3@gmail.com
<br>Пароль: willumye
<br>Порт: 8082</b>
<br><img src="img/img02.png">
<br><img src="img/img03.png">
8. С помощью команды sudo docker `inspect postgres-dc` (где postgres-dc - имя контейнера с БД) узнаем IP-адрес сервера БД.
<br><img src="img/img04.png">
9. Создаем сервер
<br><img src="img/img05.png">
<br>Имя пользователя по-умолчанию `postgres`
<br>Пароль из файла <a href="docker-compose.yml">docker-compose.yml</a>
10. Все. Связка PostgreSQL + pgAdmin развернута
<br><img src="img/img06.png">

## 2. Загрузка сценария 
1. Создаем новую БД school21
2. Грузим сценарий <a href="model.sql">model.sql</a> (Можно просто скопировать содержимое файла и вставить в раздел `Запросник SQL`)
   <br><img src="img/img0201.png">
3. Видим, что наши таблицы успешно сформировались
   <br><img src="img/img0202.png">

## 3. Подключение Visual Code  к вашей БД (необязательно)

Без слов:
<br><img src="img/sql01.png">
<br><img src="img/sql02.png">
<br><img src="img/sql03.png">

## 4. Схема БД
<br><img src="img/schema.png">
