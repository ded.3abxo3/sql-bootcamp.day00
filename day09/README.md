## Скрипт для создания директорий ex00-exNN

<a href=mkalldirs.sh>mkalldirs.sh</a>

## Инструкция по развертыванию виртуальной машины + PostgreSQL/pgAdmin в контейнерах Docker

<a href="https://gitlab.com/school218063421/sql-projects/s21-sql-bootcamp/-/blob/main/README.md" target=_blank>ЖМАКНИ СЮДА</a>

## Запросы
- ex00
  ```sql
  -- DROP TABLE IF EXISTS person_audit;
  CREATE TABLE person_audit (
    created TIMESTAMP WITH TIME zone NOT NULL DEFAULT current_timestamp,
    type_event char(1) DEFAULT 'I',
    row_id bigint NOT NULL,
    name varchar,
    age integer,
    gender varchar,
    address varchar,
    CONSTRAINT ch_type_event CHECK (type_event IN('I', 'U', 'D'))
  );

  CREATE OR REPLACE FUNCTION fnc_trg_person_insert_audit() RETURNS trigger AS $person_audit$
  BEGIN
    INSERT INTO person_audit(type_event,row_id,name,age,gender,address)
    VALUES('I',new.id, new.name, new.age, new.gender, new.address);
    RETURN NULL;
  END;
  $person_audit$ LANGUAGE plpgsql;

  -- Создаем триггер на добавление данных в таблицу персон с автозапуском триггера на добавление в аудит
  CREATE OR REPLACE TRIGGER trg_person_insert_audit AFTER INSERT ON person
    FOR EACH ROW EXECUTE FUNCTION fnc_trg_person_insert_audit();

  INSERT INTO person(id, name, age, gender, address) VALUES (10,'Damir', 22, 'male', 'Irkutsk');
  SELECT * FROM person_audit;
  ```
- ex01
  ```sql

  CREATE OR REPLACE FUNCTION fnc_trg_person_update_audit() RETURNS TRIGGER AS $person_audit$
  BEGIN
    INSERT INTO person_audit(type_event,row_id,name,age,gender,address)
    VALUES('U',old.id, old.name, old.age, old.gender, old.address);
    RETURN NULL;
  END;
  $person_audit$ LANGUAGE plpgsql;
  -- Создаем триггер на срабатывание функции выше при каждом апдейте таблицы персон
  CREATE OR REPLACE TRIGGER trg_person_update_audit AFTER UPDATE ON person
    FOR EACH ROW EXECUTE FUNCTION fnc_trg_person_update_audit();

  -- Проверка:
  UPDATE person SET name = 'Bulat' WHERE id = 10; 
  UPDATE person SET name = 'Damir' WHERE id = 10;
  SELECT * FROM person_audit;
  ```
- ex02
  ```sql
  CREATE OR REPLACE FUNCTION fnc_trg_person_delete_audit() RETURNS trigger AS $person_audit$
  BEGIN
    INSERT INTO person_audit(type_event,row_id,name,age,gender,address)
    VALUES('D',old.id, old.name, old.age, old.gender, old.address);
    RETURN NULL;
  END;
  $person_audit$ LANGUAGE plpgsql;
  -- Триггер на удаление данных из персон
  CREATE OR REPLACE TRIGGER trg_person_delete_audit AFTER DELETE ON person
    FOR EACH ROW EXECUTE FUNCTION fnc_trg_person_delete_audit();

  DELETE FROM person WHERE id = 10;
  SELECT * FROM person_audit;
  ```
- ex03
  ```sql
  -- Дропаем старые триггеры
  DROP TRIGGER IF EXISTS trg_person_insert_audit ON person;
  DROP TRIGGER IF EXISTS trg_person_update_audit ON person;
  DROP TRIGGER IF EXISTS trg_person_delete_audit ON person;

  DROP FUNCTION IF EXISTS fnc_trg_person_delete_audit;
  DROP FUNCTION IF EXISTS fnc_trg_person_insert_audit;
  DROP FUNCTION IF EXISTS fnc_trg_person_update_audit;

  DROP TRIGGER IF EXISTS trg_person_audit ON person;
  DROP FUNCTION IF EXISTS fnc_trg_person_audit;

  -- TG_OP - аварийная системная переменная, которая содержит тип операции, вызвавшей триггер
  CREATE OR REPLACE FUNCTION fnc_trg_person_audit() RETURNS trigger AS $person_audit$
  BEGIN
    IF (TG_OP = 'INSERT') THEN
        INSERT INTO person_audit(type_event,row_id,name,age,gender,address)
        VALUES('I',new.id, new.name, new.age, new.gender, new.address);
        RETURN NULL;
    ELSIF (TG_OP = 'UPDATE') THEN
        INSERT INTO person_audit(type_event,row_id,name,age,gender,address)
        VALUES('U',old.id, old.name, old.age, old.gender, old.address);
        RETURN NULL;
    ELSIF (TG_OP = 'DELETE') THEN
        INSERT INTO person_audit(type_event,row_id,name,age,gender,address)
        VALUES('D',old.id, old.name, old.age, old.gender, old.address);
    END IF;    
    RETURN NULL;
  END;
  $person_audit$ LANGUAGE plpgsql;
  -- Создаем новый триггер для новой функции
  CREATE OR REPLACE TRIGGER trg_person_audit AFTER INSERT OR DELETE OR UPDATE ON person
    FOR EACH ROW EXECUTE FUNCTION fnc_trg_person_audit();


  INSERT INTO person(id, name, age, gender, address) VALUES (10,'Damir', 22, 'male', 'Irkutsk'); 
  UPDATE person SET name = 'Bulat' WHERE id = 10; UPDATE person SET name = 'Damir' WHERE id = 10; 
  DELETE FROM person WHERE id = 10;

  SELECT * FROM person_audit;
  ```
- ex04
  ```sql
  CREATE OR REPLACE FUNCTION fnc_persons_female()
  RETURNS TABLE (
        id bigint,
        name varchar,
        age integer,
        gender varchar,
        address varchar
  ) AS $$
        (SELECT * FROM person P WHERE P.gender = 'female');
  $$ LANGUAGE sql;

  CREATE OR REPLACE FUNCTION fnc_persons_male()
  RETURNS TABLE (
        id bigint,
        name varchar,
        age integer,
        gender varchar,
        address varchar
  ) AS $$
        (SELECT * FROM person P WHERE P.gender = 'male');
  $$ LANGUAGE sql;

  SELECT *
  FROM fnc_persons_male();

  SELECT *
  FROM fnc_persons_female();
  ```
- ex05
  ```sql
  CREATE OR REPLACE FUNCTION fnc_persons(pgender varchar='female')
  RETURNS TABLE (
        id bigint,
        name varchar,
        age integer,
        gender varchar,
        address varchar
  ) AS $$
        (SELECT * FROM person P WHERE P.gender = pgender);
  $$ LANGUAGE sql;
  
  -- Явно задаем переменную pgender
  SELECT *
  FROM fnc_persons(pgender:='male');
  --  По умолчанию female
  SELECT *
  FROM fnc_persons();
  ```
- ex06
  ```sql
  CREATE OR REPLACE FUNCTION fnc_person_visits_and_eats_on_date(pperson varchar='Dmitriy',pprice int=500, pdate date='2022-01-08')
  RETURNS TABLE (
        pizzeria_name varchar
  ) AS $$
        (SELECT DISTINCT Z.name FROM person_visits V
            JOIN person P on P.id=V.person_id
            JOIN pizzeria Z on Z.id=V.pizzeria_id
            JOIN menu M on M.pizzeria_id=Z.id
        WHERE P.name = pperson AND V.visit_date = pdate AND M.price < pprice)
  $$ LANGUAGE sql;

  -- Дмитрий посещал и заказал пиццу за 800 2022-01-08 (имя и дата заданы по-умолчанию)
  select *
  from fnc_person_visits_and_eats_on_date(pprice := 800);

  -- Сменили всю палитру переменных
  select *
  from fnc_person_visits_and_eats_on_date(pperson := 'Anna',pprice := 1300,pdate := '2022-01-01');
  ```
- ex07
  ```sql
  CREATE OR REPLACE FUNCTION func_minimum(VARIADIC arr NUMERIC[])
  RETURNS NUMERIC AS $$
    SELECT min(arr[i]) FROM generate_subscripts(arr, 1) g(i);
  $$ LANGUAGE SQL;

  SELECT func_minimum(VARIADIC arr => ARRAY[10.0, -1.0, 5.0, 4.4]);
  ```
- ex08
  ```sql
  CREATE OR REPLACE FUNCTION fnc_fibonacci(pstop int DEFAULT 10) 
  RETURNS TABLE(num int) AS $$ 
    WITH RECURSIVE cte_fibo(n1, n2) AS (
        VALUES (0, 1)
        UNION ALL
        SELECT n2,
            n1 + n2
        FROM cte_fibo
        WHERE n2 < pstop)
    SELECT n1  FROM cte_fibo;
  $$ LANGUAGE SQL;

  -- Макс значение 1000000000
  select *
  from fnc_fibonacci(1000000000);

  -- По умолчанию не больше 10 (задано дефолтом в функции)
  select *
  from fnc_fibonacci();
  ```

