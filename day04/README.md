## Скрипт для создания директорий ex00-exNN

<a href=mkalldirs.sh>mkalldirs.sh</a>

## Инструкция по развертыванию виртуальной машины + PostgreSQL/pgAdmin в контейнерах Docker

<a href=../README.md>../README.md</a>

## Запросы
- ex00
  ```sql
  CREATE VIEW v_persons_female AS
    SELECT * FROM person WHERE person.gender = 'female';
  ```
  ```sql
  CREATE VIEW v_persons_male AS
    SELECT * FROM person WHERE person.gender = 'male';
  ```
- ex01
  ```sql
  SELECT name FROM v_persons_male
  UNION
  SELECT name from v_persons_female
  ORDER BY name;
  ```
- ex02
  ```sql
  CREATE VIEW v_generated_dates AS
    SELECT days::date AS g_days
    FROM generate_series('2022-01-01'::date, '2022-01-31'::date, interval '1 day') as days
    ORDER BY g_days;
  ```
- ex03
  ```sql
  SELECT DISTINCT g_days AS missing_dates 
  FROM v_generated_dates GD
    LEFT JOIN person_visits PV ON GD.g_days = PV.visit_date
  WHERE PV.visit_date IS NULL
  ORDER BY missing_dates;
  ```
- ex04
  ```sql
  CREATE VIEW v_symmetric_union AS (
    (
        SELECT person_id FROM person_visits WHERE visit_date = '2022-01-02'
        EXCEPT
        SELECT person_id FROM person_visits WHERE visit_date = '2022-01-06'
    )
    UNION
    (
        SELECT person_id FROM person_visits WHERE visit_date = '2022-01-06'
        EXCEPT
        SELECT person_id FROM person_visits WHERE visit_date = '2022-01-02'
    )
  );
  ```
- ex05
  ```sql
  CREATE VIEW v_price_with_discount AS
    SELECT 
        PRS.name, 
        MN.pizza_name, 
        MN.price, 
        CAST(price - price * 0.1 AS INTEGER) AS discount_price
    FROM person PRS
        JOIN person_order PO ON PRS.id = PO.person_id
        JOIN menu MN ON PO.menu_id = MN.id
  ORDER BY PRS.name, MN.pizza_name;
  ```
- ex06
  ```sql
  CREATE MATERIALIZED VIEW mv_dmitriy_visits_and_eats AS
    SELECT PZR.name AS pizzeria_name
    FROM pizzeria PZR
        JOIN person_visits PVS ON PZR.id = PVS.pizzeria_id
        JOIN person PRS ON PVS.person_id = PRS.id
        JOIN menu MN ON PZR.id = MN.pizzeria_id
    WHERE PVS.visit_date = '2022-01-08' AND PRS.name LIKE 'Dmitriy' AND MN.price < 800
    ORDER BY PZR.name;
  ```
- ex07
  ```sql
  INSERT INTO 
    person_visits (id, person_id, pizzeria_id, visit_date) 
  VALUES
    (
        (SELECT MAX(id) FROM person_visits) + 1,
        (SELECT id FROM person WHERE name LIKE 'Dmitriy'),
        (SELECT DISTINCT PZ.id FROM pizzeria PZ
            JOIN mv_dmitriy_visits_and_eats DV ON DV.pizzeria_name <> PZ.name
            JOIN menu MN ON MN.pizzeria_id = PZ.id
        WHERE MN.price < 800 LIMIT 1),
        '2022-01-08'
    );
  ```
  ```sql
  REFRESH MATERIALIZED VIEW mv_dmitriy_visits_and_eats;
  ```
- ex08
  ```sql
  DROP VIEW v_persons_female;
  DROP VIEW v_persons_male;
  DROP VIEW v_price_with_discount;
  DROP VIEW v_generated_dates;
  DROP VIEW v_symmetric_union;
  DROP MATERIALIZED VIEW mv_dmitriy_visits_and_eats;
  ```
