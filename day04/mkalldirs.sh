#!/bin/bash

echo "Enter max dir number"
read LAST_DIR

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
SCRIPT_NAME=$(basename "$0")
DIR_NAME=$(basename "$SCRIPT_DIR")

# Dirs create from ex00 to exXX
for i in $(seq -f "%02g" 0 $LAST_DIR); do
		   cur_dir=ex${i}
		   FILE_NAME="${DIR_NAME}_ex${i}.sql"
		   mkdir -p $cur_dir
		   touch "$cur_dir/$FILE_NAME"
done

echo "All directories & all files are created"
